// pages/shop/genre/index.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    productPage:{}
  },
  switchTab:function(e){
    let queryType = e.target.dataset.querytype
    this.setData({
      queryType: queryType
    })
    this.getProductList(queryType)
  },
  getProductList:function(queryType) {
    var that = this
    wx.request({
      url: app.globalData.API_URL + "getProductPage", //仅为示例，并非真实的接口地址
      data: {
        pageNum: 1,
        pageSize: 20,
        queryType: queryType
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        if (res.data.code == 0) {
          that.setData({
            productPage: res.data.data
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      queryType:options.queryType
    })
    this.getProductList(options.queryType)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})